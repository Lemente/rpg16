BEGIN {
    header_count = 0;
    line_count = 0;
}

/^#/ {
    header_count++;
}

!/^#/ {
    if (header_count == 1 && line_count == 0) {
        print;
        line_count++;
    }
}
