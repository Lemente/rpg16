#!/bin/bash

# Set the directories to output to
if [ -z "${EXPORT_DIR}" ]; then
    EXPORT_DIR=build
fi
if [ -z "${TMP_DIR}" ]; then
    TMP_DIR=tmp
fi

# Export textures from a folder to ${EXPORT_DIR},
# converting .gif textures to the standard spritesheet format
function folderExport() {
    echo "Exporting folder ${1}..."
    rsync -r --exclude="*.gif" --exclude="*.txt" "${1}" "${EXPORT_DIR}"

    for i in `find "${1}" -name "*.gif" -print`; do
        mkdir -p "${EXPORT_DIR}/$(dirname "${i}")"
        montage "${i}" -tile 1x -geometry '1x1+0+0<' -alpha set -background transparent -coalesce -quality 100 "${EXPORT_DIR}/${i%.gif}.png" || { echo "Failed to export ${i}"; exit 1; }
    done
}

# Copy a file to ${EXPORT_DIR}
function copyExport() {
    rsync "${1}" "${EXPORT_DIR}"
}

# Copy textures from a clone file
#
# Clone file format:
# relative/path/to/source,relative/path/to/destination
# # Comment
#
# Paths from this file are relative to ${EXPORT_DIR}
# Empty lines are also allowed
function makeClones() {
    while read i; do
        if [[ -n ${i} && ${i::1} != '#' ]]; then
            dest="${EXPORT_DIR}/${i#*,}"
            mkdir -p `dirname ${dest}`
            rsync ${EXPORT_DIR}/${i%,*} ${dest}
        fi
    done < ${1}
}

# Set texture alphas from a file
#
# Alpha file format:
# relative/path/to/destination,alpha
# #Comment
#
# Paths from this file are relative to ${EXPORT_DIR}
# Empty lines are also allowed
function makeAlphas() {
    while read i; do
        if [[ -n ${i} && ${i::1} != '#' ]]; then
            mogrify -type TrueColorAlpha -alpha set -channel A -evaluate min ${i#*,} "${EXPORT_DIR}/${i%,*}"
        fi
    done < ${1}
}

# Ensure that the build directory exists
mkdir -p "${EXPORT_DIR}"

# Create screenshot.png from source
screenshot_file="screenshot.tiff"
echo "Export ${screenshot_file}..."
#TODO: Generate the image?
convert "${screenshot_file}" -flatten "${EXPORT_DIR}/screenshot.png" || { echo "Failed to export ${1}"; exit 1; }

# Copy texture pack files
echo "Copy text files..."
copyExport "CHANGELOG.md"
copyExport "LICENSE.txt"
copyExport "override.txt"
copyExport "README.md"
copyExport "texture_pack.conf"

echo "Export textures..."
# Copy images, converting any source files
folderExport "3d_armor"
folderExport "old/awards"
folderExport "old/baked_clay"
folderExport "basic_materials"
folderExport "beds"
folderExport "binoculars"
folderExport "boats"
folderExport "bones"
folderExport "bucket"
folderExport "butterflies"
folderExport "carts"
folderExport "caverealms"
folderExport "darks"
makeAlphas "darks/alphas.txt"
folderExport "default"
makeAlphas "default/alphas.txt"
folderExport "doors"
folderExport "dye"
folderExport "email"
folderExport "old/ethereal"
folderExport "farming"
folderExport "fire"
folderExport "fireflies"
folderExport "flowers"
folderExport "grenades_basic"
folderExport "gui"
folderExport "handholds"
folderExport "hbarmor"
folderExport "hbhunger"
folderExport "hbsprint"
folderExport "hudbars"
folderExport "lavastuff"
folderExport "magma_conduits"
folderExport "map"
folderExport "minetest_wadsprint"
folderExport "misc"
folderExport "old/mobs"
folderExport "old/mobs_monster"
folderExport "moon_phases"
makeClones "moon_phases/clones.txt"
folderExport "moreblocks"
folderExport "moreores"
folderExport "moretrees"
folderExport "multitools"
folderExport "player_api"
folderExport "screwdriver"
folderExport "shooter/shooter"
folderExport "shooter/shooter_crossbow"
folderExport "shooter/shooter_flaregun"
folderExport "shooter/shooter_grenade"
folderExport "shooter/shooter_guns"
folderExport "shooter/shooter_hook"
folderExport "shooter/shooter_rocket"
folderExport "shooter/shooter_turret"
folderExport "stairs"
folderExport "stamina"
folderExport "tnt"
folderExport "vessels"
folderExport "wool"
folderExport "xpanes"

# Handle grenades_basic clones late since it depends on shooter_grenade and tnt
makeClones "grenades_basic/clones.txt"

# CTF
folderExport "old/ctf/ctf_bandages"
folderExport "old/ctf/ctf_classes"
folderExport "old/ctf/ctf_colors"
folderExport "old/ctf/ctf_events"
folderExport "old/ctf/ctf_flag"
folderExport "old/ctf/ctf_map"
folderExport "old/ctf/ctf_traps"
folderExport "old/ctf/medkits"
folderExport "old/ctf/sniper_rifles"
mkdir -p "${EXPORT_DIR}/ctf/grenades" # Folder for CTF grenades
makeClones "old/ctf/clones.txt"

# Repixture
folderExport "old/repixture/armor"
folderExport "old/repixture/bed"
folderExport "old/repixture/default"
folderExport "old/repixture/door"
folderExport "old/repixture/farming"
folderExport "old/repixture/gold"
folderExport "old/repixture/headbars"
folderExport "old/repixture/hunger"
folderExport "old/repixture/jewels"
folderExport "old/repixture/locks"
folderExport "old/repixture/lumien"
folderExport "old/repixture/mobs"
folderExport "old/repixture/music"
folderExport "old/repixture/nav"
folderExport "old/repixture/parachute"
folderExport "old/repixture/partialblocks"
folderExport "old/repixture/player_skins"
folderExport "old/repixture/ui"
folderExport "old/repixture/village"
folderExport "old/repixture/weather"
folderExport "old/repixture/wieldhand"
makeAlphas "old/repixture/default/alphas.txt"
makeClones "old/repixture-clones.txt"

echo "Done! Files copied to ${EXPORT_DIR}"
