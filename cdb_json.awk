BEGIN {
    short_description = ENVIRON["SHORT_DESCRIPTION"];
    long_description = ENVIRON["LONG_DESCRIPTION"];
}

/^SHORT_DESCRIPTION/ {
    print "    \"short_description\": \"" short_description "\",";
}

/^LONG_DESCRIPTION/ {
    print "    \"long_description\": \"" long_description "\"";
}

!/DESCRIPTION/ {
    print;
}
